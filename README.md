# GladiatorMine!!![picsart_03-13-05 28 27](https://cloud.githubusercontent.com/assets/16944178/13729941/22a59b12-e941-11e5-801e-48b322fa29c2.jpg)
# GLADIATORMINE BETA #1 IS OUT!!
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Notices:
- GladiatorMine supports MCPE 0.14.0 by providing an 0.14.0 build. You can checkout the [mcpe-0.14 build here] (https://github.com/enderManiacsKingdom/GladiatorMine/).

## Project Addons:
- [Official Forums](http://FORUMS.GLADIATORMINE.NET)
- [ReadTheDocs Build (NOT COMPLETE!!)](http://www.gladiatormine.net/ReadTheDocs/)

## Notes

- The GladiatorMine App orgianly uses php5 builds of gladiatormine, so we may provide a php5 branch,if not already the gladiatormine has php7 compatabilty!! (WE WILL KEEP U UP TO DATE ON THAT INFO ABOUT THE APP!!)
- This is a third-party build of [PocketMine-MP](https://github.com/PocketMine/PocketMine-MP). GladiatorMine is in no way affiliated with [PocketMine-MP](https://github.com/PocketMine/PocketMine-MP).
- Please read the [contributing guidelines](https://github.com/EnderManiacsKingdom/GladiatorMine/branch/php7-0.14/CONTRIBUTING.md) before submitting an issue or a pull request. Any contributions that don't match them will be closed immediately by a team member.
- **Please do not submit issues about Jenkins or any third-party services relating to GladiatorMine. They will be closed immediately by a team member if you do.**
- Please do not use the issue tracker as a chat room. If you want to chat and ask questions, contact [@NetherManiacsKingdom on Google+](https://plus.google.com/communities/103024950811996154312) or join the [official forums](http://forums.gladiatormine.net).
- We're proud to say that GladiatorMine has the best redstone system so far. Thanks for all your support!

## Acknowledgements

- The original code in GladiatorMine is from [PocketMine-MP](https://github.com/PocketMine/PocketMine-MP). All original code structure and base was written by the [PocketMine Team](https://github.com/PocketMine).

## What is this?

GladiatorMine is software for hosting Minecraft : Pocket Edition servers. From controlling everything you can possibly do on MCPE to everything players can ever imagine on a server, GladiatorMine is the perfect solution to hosting large-scale MCPE servers. Our goal is to make GladiatorMine the best MCPE server software out there, and can implement the latest features from the singleplayer MCPE version.

## Why is the GladiatorMine team doing this?

Because Shoghi of the PocketMine team has given us so much, we've decided to build onto the project and help out the community. Thanks to all the team members of the NetherManiacsKingdom Pocketmine Plugin Development Team, the work you are seeing today is open-source and free for every human to use. It'll stay free forever and the team will try and keep it active as well as possible.

## Can I help out the team and contribute to GladiatorMine?

Why not? We're always looking for developers to help out with the project! Make sure you check out the [Contributing Guidelines](https://github.com/EnderManiacsKingdom/GladiatorMine/blob/php7-0.14/CONTRIBUTING.md) first though.

## Can I join the GladiatorMine community?

Of course you can! Check out the [GladiatorMine forums](http://FORUMS.gladiatormine.net/) now!

## How do I install GladiatorMine?

Read the documentation for installing GladiatorMine (NOT COMPLETE) [here](http://www.gladiatormine.net/ReadTheDocs/).


## I'm on a MCPE server hosting provider. How can I install GladiatorMine from there?

No MCPE server hosting providers support GladiatorMine as a version for their customers to download yet. The following list shows all of the providers who support GladiatorMine as a version to download for use on their servers:

# No Hosting Provider's Work With GladiatorMine Yet!!

**If you are a MCPE server hosting provider and you support GladiatorMine as a version available for download and to use as part of your service, contact us and we'll happily add your business to the list.**

## What third-party libraries or protocols does GladiatorMine use?

Here's the complete list:
* __[PHP Sockets](http://php.net/manual/en/book.sockets.php)__
* __[PHP SQLite3](http://php.net/manual/en/book.sqlite3.php)__
* __[PHP BCMath](http://php.net/manual/en/book.bc.php)__
* __[PHP pthreads](http://pthreads.org/)__ by _[krakjoe](https://github.com/krakjoe)_: Threading for PHP - Share Nothing, Do Everything.
* __[PHP YAML](https://code.google.com/p/php-yaml/)__ by _Bryan Davis_: The Yaml PHP Extension provides a wrapper to the LibYAML library.
* __[LibYAML](http://pyyaml.org/wiki/LibYAML)__ by _Kirill Simonov_: A YAML 1.1 parser and emitter written in C.
* __[cURL](http://curl.haxx.se/)__: cURL is a command line tool for transferring data with URL syntax
* __[Zlib](http://www.zlib.net/)__: A Massively Spiffy Yet Delicately Unobtrusive Compression Library
* __[Source RCON Protocol](https://developer.valvesoftware.com/wiki/Source_RCON_Protocol)__
* __[UT3 Query Protocol](http://wiki.unrealadmin.org/UT3_query_protocol)__

## Quick Links

* __[Homepage] (http://www.gladiatormine.net/)__
* __[Forums](http://FORUMS.gladiatormine.net/)__
* __[Documentation](http://www.gladiatormine.net/ReadTheDocs/)__
* __[Plugin Repository](http://forums.gladiatormine.net/index.php?resources/)__
* __[Official Google+ Account](https://plus.google.com/communities/103024950811996154312)__
